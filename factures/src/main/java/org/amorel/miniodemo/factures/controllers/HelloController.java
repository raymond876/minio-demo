package org.amorel.miniodemo.factures.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/factures/hello")
	public String index() {
		return "Greetings from Facture !";
	}

}