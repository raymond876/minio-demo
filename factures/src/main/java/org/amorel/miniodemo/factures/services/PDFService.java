package org.amorel.miniodemo.factures.services;

import org.amorel.miniodemo.factures.models.Facture;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.xml.catalog.CatalogFeatures.Feature;

import java.awt.Color;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;

@Service
public class PDFService {

  private void writeTableHeader(PdfPTable table) {
    PdfPCell cell = new PdfPCell();
    cell.setBackgroundColor(Color.BLUE);
    cell.setPadding(5);

    Font font = FontFactory.getFont(FontFactory.HELVETICA);
    font.setColor(Color.WHITE);

    cell.setPhrase(new Phrase("User ID", font));

    table.addCell(cell);

    cell.setPhrase(new Phrase("E-mail", font));
    table.addCell(cell);

    cell.setPhrase(new Phrase("Full Name", font));
    table.addCell(cell);

    cell.setPhrase(new Phrase("Roles", font));
    table.addCell(cell);

    cell.setPhrase(new Phrase("Enabled", font));
    table.addCell(cell);
  }

  private void writeTableData(PdfPTable table, Facture facture) {
    table.addCell(String.valueOf(facture.id));
    table.addCell(facture.client);
    table.addCell("tyutyutyu");
    table.addCell("hjhkhkj");
    table.addCell("jkhkjhkj");
  }

  public void export(OutputStream out, Facture facture) throws DocumentException, IOException {
    Document document = new Document(PageSize.A4);
    PdfWriter.getInstance(document, out);

    document.open();
    Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
    font.setSize(18);
    font.setColor(Color.BLUE);

    Paragraph p = new Paragraph("List of Users", font);
    p.setAlignment(Paragraph.ALIGN_CENTER);

    document.add(p);

    PdfPTable table = new PdfPTable(5);
    table.setWidthPercentage(100f);
    table.setWidths(new float[] { 1.5f, 3.5f, 3.0f, 3.0f, 1.5f });
    table.setSpacingBefore(10);

    writeTableHeader(table);
    writeTableData(table, facture);

    document.add(table);

    document.close();

  }
}
