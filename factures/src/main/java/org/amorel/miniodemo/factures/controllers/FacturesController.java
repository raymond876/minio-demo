package org.amorel.miniodemo.factures.controllers;

import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletResponse;

import org.amorel.miniodemo.factures.services.ObjectsStorageService;
import org.amorel.miniodemo.factures.services.PDFService;
import org.amorel.miniodemo.factures.models.Facture;

@RestController
public class FacturesController {

	@Autowired
	PDFService pdfservice;

	@Autowired
	ObjectsStorageService objectstorageService;

	@GetMapping("/factures")
	public List<Facture> index() {

		List<Facture> list = new ArrayList<Facture>();

		for (Integer i = 0; i <= 5; i++) {
			list.add(new Facture());
		}
		return list;
	}

	@GetMapping("/factures/pdf")
	public void index(HttpServletResponse response) throws IOException {

		Facture facture = new Facture();

		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		// String headerValue = "attachment; filename=users_" + currentDateTime +
		// ".pdf";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);

		// pdfservice.export(response.getOutputStream(), facture);
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		pdfservice.export(os, facture);

		objectstorageService.UploadStream(new ByteArrayInputStream(os.toByteArray()));

	}

}